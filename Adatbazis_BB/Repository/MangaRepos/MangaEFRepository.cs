﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adatbazis_BB;
using Repository.GenericRepos;

namespace Repository.MangaRepos
{
    public class MangaEFRepository : EFRepository<MANGA>, IMangaRepository
    {
        public MangaEFRepository(DbContext newcontext) : base(newcontext)
        {
        }

        public override MANGA GetById(int id)
        {
            return Get(akt => akt.MANGAID == id).SingleOrDefault();
        }
    }
}

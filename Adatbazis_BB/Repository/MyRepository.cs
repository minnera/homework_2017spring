﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Repository.MangakaRepos;
using Repository.MangaRepos;
using Repository.MTConnRepos;
using Repository.PublisherRepos;
using Repository.TranslaterRepos;

namespace Repository
{
    public class MyRepository
    {
        public IMangaRepository MRepo { get; private set; }
        public IMangakaRepository MKRepo { get; private set; }
        public ITranslaterRepository TRepo { get; private set; }
        public IMTConnRepository CRepo { get; private set; }
        public IPublisherRepository PRepo { get; private set; }

        public MyRepository(IMangaRepository MR, IMangakaRepository MKR, IMTConnRepository CR, IPublisherRepository PR,
            ITranslaterRepository TR)
        {
            MRepo = MR;
            MKRepo = MKR;
            TRepo = TR;
            CRepo = CR;
            PRepo = PR;
        }
    }
}

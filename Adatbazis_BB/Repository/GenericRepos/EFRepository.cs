﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Repository.GenericRepos
{
    //Ez tábláktól független, ezért abstract, entity framework tároló
    abstract public class EFRepository<TEntity> : IRepository<TEntity>
        where TEntity:class //ezeket a megszorításokat mindig meg kell ismételni a felvett interfacektől
    {
        //egy ősosztályt fogunk használni tároló jelzésére
        protected DbContext context;

        public EFRepository(DbContext newcontext)
        {
            context = newcontext;
        }

        //ez egy bonyolult dolog
        //ez táblafüggetlenül nemigazán oldható meg, nem csináljuk meg itt.
        abstract public TEntity GetById(int id);


        //felszabadítás, ezt kb sosem akarjuk meghívni, az rossz kód ahol meghívódik, vagy átmeneti dolog
        public void Dispose()
        {
            context.Dispose();
        }

        //beillesztés
        public void Insert(TEntity newentity)
        {
            context.Set<TEntity>().Add(newentity); //ezzel módosul a státusz is a db-ben
            //a set azthiszem odaadja nekünk a context tartalmát, hogy elérhessük és dolgozhassunk vele
            //mert itt a set szó halmazt jelent
            //az add ehhez hozzáadja a megadott elemet
            context.SaveChanges();//ezzel jelezzük, hogy a változtatásokat a contexten frissítse bele a dbbe
        }

        public void Delete(int id)
        {
            //kikeressük az id alapján a törlendő entityt
            TEntity oldentity = GetById(id);
            //megnézzük volt e entity az id alatt
            if (oldentity == null)
            {
                throw new ArgumentException("NO DATA");
            }
            //ha nem kaptunk kivételt, akkor ez a törlés lefut
            Delete(oldentity);
        }

        public void Delete(TEntity oldentity)
        {
            //töröljük a megadott entityt
            context.Set<TEntity>().Remove(oldentity);

            context.SaveChanges();
        }

        //
        public IQueryable<TEntity> GetAll()
        {
            return context.Set<TEntity>();
            //visszaadjuk a context halmazát.
        }

        public IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> condition)
        {
            //vesszük az összes elemet, és azokból kiválogatjuk azt, ami nekünk kell
            return GetAll().Where(condition);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Repository.GenericRepos
{
    //CRUD - create, read, update, delete
    public interface IRepository<TEntity> : IDisposable/*ezzel disposálható/eldobható/törölhető lesz*/ 
        where TEntity:class
    {
        void Insert(TEntity newentity); //megadott elem beillesztése metódus
        void Delete(int id); //megadott id helyen törlő metódus
        void Delete(TEntity oldentity); //megadott elem törlése metódus

        TEntity GetById(int id); //visszaadja a megadott id-jű elemet

        IQueryable<TEntity> GetAll(); //visszaadja az összes elemet, IQueryable módon, egy tömböt ad vissza

        IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> condition); //vissza ad egy tömböt iqueryable módon a megadott funkció segítségével
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adatbazis_BB;
using Repository.GenericRepos;

namespace Repository.MTConnRepos
{
    public class MTConnEFRepository : EFRepository<MTCONN>, IMTConnRepository
    {
        public MTConnEFRepository(DbContext newcontext) : base(newcontext)
        {
        }

        public override MTCONN GetById(int id)
        {
            return Get(akt => akt.CONNID == id).SingleOrDefault();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adatbazis_BB;
using Repository.GenericRepos;

namespace Repository.MangakaRepos
{
    public class MangakaEFRepository : EFRepository<MANGAKA>, IMangakaRepository
    {
        public MangakaEFRepository(DbContext newcontext) : base(newcontext)
        {
        }

        public override MANGAKA GetById(int id)
        {
            return Get(akt => akt.MANGAKAID == id).SingleOrDefault();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adatbazis_BB;
using Repository.GenericRepos;

namespace Repository.PublisherRepos
{
    public class PublisherEFRepository : EFRepository<PUBLISHER>, IPublisherRepository
    {
        //mindig kell ctor-t írni, egyelőre nem kell az alap ctor-t változtatni
        public PublisherEFRepository(DbContext newcontext) : base(newcontext)
        {
        }

        public override PUBLISHER GetById(int id)
        {
            //SingleOrDefault: ha nincs semmi az id helyén, akkor default értéket ad vissza.
            //akt : random változó név, olyan mint foreach-ben a var v, ez a neve az épp soron következő elemnek, amit néz
            return Get(akt => akt.PUBLISHERID == id).SingleOrDefault();
        }
    }
}

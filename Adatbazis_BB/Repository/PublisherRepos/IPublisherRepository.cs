﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adatbazis_BB;
using Repository.GenericRepos;

namespace Repository.PublisherRepos
{
    public interface IPublisherRepository : IRepository<PUBLISHER>
    {
        //ide írnánk az extra dolgokat, amik az IRepositoryn kívűl kellenének, most nincs ilyen
        //esetleg lehet Modify-t adni, ha azt akarjuk, hogy egy elemet lehessen változtatni.
        
    }
}

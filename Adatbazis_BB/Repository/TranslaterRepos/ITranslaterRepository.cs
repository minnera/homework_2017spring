﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adatbazis_BB;
using Repository.GenericRepos;

namespace Repository.TranslaterRepos
{
    public interface ITranslaterRepository : IRepository<TRANSLATER>
    {
        //skill +1
        void Modify(int id);
        //language change
        void Modify(int id, int newskill, string newlanguage);
        //name change
        void Modify(int id, string newname);
        //all change
        void Modify(int id, string name, int skill, string language);
    }
}

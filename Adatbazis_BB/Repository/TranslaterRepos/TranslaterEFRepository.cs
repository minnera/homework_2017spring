﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adatbazis_BB;
using Repository.GenericRepos;

namespace Repository.TranslaterRepos
{
    public class TranslaterEFRepository : EFRepository<TRANSLATER>, ITranslaterRepository
    {
        public TranslaterEFRepository(DbContext newcontext) : base(newcontext)
        {
        }

        public override TRANSLATER GetById(int id)
        {
            return Get(akt => akt.TRANID == id).SingleOrDefault();
        }

        public void Modify(int id)
        {
            TRANSLATER akt = GetById(id);
            if (akt == null) { throw new ArgumentException("NO DATA"); }
            akt.TSKILL++;
            context.SaveChanges();
        }
        public void Modify(int id, int newskill, string newlanguage)
        {
            TRANSLATER akt = GetById(id);
            if (akt == null) { throw new ArgumentException("NO DATA"); }
            
            akt.TSKILL = newskill;
            
            if (newlanguage != null)
            {
                akt.TLANGUAGE = newlanguage;
            }
            context.SaveChanges();
        }
        public void Modify(int id, string newname)
        {
            //csinálunk egy elemet a translaterből, és átadjuk neki a módosítandót
            TRANSLATER akt = GetById(id);
            //megnézzük volt e ilyen elem:
            if (akt == null) { throw new ArgumentException("NO DATA"); }
            //módosítjuk
            if (newname != null)
            {
                akt.TNAME = newname;
            }
            context.SaveChanges();
            //ha egyszerre több irányból nyúlnak az adatbázishoz, akkor az kivételt dob itt, azt le kéne kezelni
            //ConcurrencyException lekezelése TODO
        }

        public void Modify(int id, string name, int skill, string language)
        {
            TRANSLATER akt = GetById(id);
            if(akt == null) { throw new ArgumentException("NO DATA"); }
            if (name != null)
            {
                akt.TNAME = name;
            }
            if (language != null)
            {
                akt.TLANGUAGE = language;
            }
            
            akt.TSKILL = skill;
            
            context.SaveChanges();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adatbazis_BB;

namespace BusinessLogic
{//crud
    public interface ILogic
    {
        IQueryable<TRANSLATER> GetTranslaters();
        //get a translater
        TRANSLATER GetOneTranslater(int id);
        //creat new translater
        void AddTranslater(TRANSLATER newTranslater);
        //delete a translater
        void DelTrans(int id);
        //modify translater; +1 for skill, change language, change name
        void ModifyTranslaterSkill(int id);
        void ModifyTranslaterLang(int id, int skill, string language);
        void ModifyTranslaterName(int id, string name);
        void ModifyTranslaterAll(int id, string name, int skill, string language);
        int GetNextTransno();

        //lehetne olyan, hogy a skill-t csak úgy lehet módosítani, hogy +1
        //+ hogy ha változik a nyelv, akkor változik azzal a skill is
    }
}
//választott tábla: TRANSLATER
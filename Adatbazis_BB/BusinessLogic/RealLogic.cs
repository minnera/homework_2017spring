﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adatbazis_BB;
using Repository;
using Repository.MangakaRepos;
using Repository.MangaRepos;
using Repository.MTConnRepos;
using Repository.PublisherRepos;
using Repository.TranslaterRepos;

namespace BusinessLogic
{
    public class RealLogic : ILogic
    {
        private MyRepository Repo;

        public RealLogic()
        {
            DBEntities DB = new DBEntities();
            MangaEFRepository MR = new MangaEFRepository(DB);
            MangakaEFRepository MKR = new MangakaEFRepository(DB);
            MTConnEFRepository CR = new MTConnEFRepository(DB);
            PublisherEFRepository PR = new PublisherEFRepository(DB);
            TranslaterEFRepository TR = new TranslaterEFRepository(DB);
            Repo = new MyRepository(MR, MKR, CR, PR, TR);
        }

        public RealLogic(MyRepository newRepo)
        {
            Repo = newRepo;
        }

        public IQueryable<TRANSLATER> GetTranslaters()
        {
            return Repo.TRepo.GetAll();
        }

        public TRANSLATER GetOneTranslater(int id)
        {
            return Repo.TRepo.GetById(id);
        }

        public void AddTranslater(TRANSLATER newTranslater)
        {
            Repo.TRepo.Insert(newTranslater);
        }

        public void DelTrans(int id)
        {
            Repo.TRepo.Delete(id);
        }

        public void ModifyTranslaterSkill(int id)
        {
            Repo.TRepo.Modify(id);
        }

        public void ModifyTranslaterLang(int id, int skill, string language)
        {
            Repo.TRepo.Modify(id, skill, language);
        }

        public void ModifyTranslaterName(int id, string name)
        {
            Repo.TRepo.Modify(id, name);
        }

        public void ModifyTranslaterAll(int id, string name, int skill, string language)
        {
            Repo.TRepo.Modify(id, name, skill, language);
        }

        public int GetNextTransno()
        {
            return Repo.TRepo.GetAll().Max(x => (int)x.TRANID) + 1;
        }
    }
}

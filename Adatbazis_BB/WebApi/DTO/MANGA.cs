﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.DTO
{
    public class Manga
    {
        public int mangaid { get; set; }
        public string manganame { get; set; }
        public int mangapublisher { get; set; }
        public int manga_mangaka { get; set; }
        public DateTime publishdate { get; set; }
        public string mlanguage { get; set; }

    }
}
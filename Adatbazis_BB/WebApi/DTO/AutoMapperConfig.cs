﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;

namespace WebApi.DTO
{
    public class AutoMapperConfig
    {
        public static IMapper GetMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                //össze párosít két osztályt.
                cfg.CreateMap<Adatbazis_BB.TRANSLATER, Models.TranslaterModel>().ReverseMap();
                //cfg.CreateMap<Adatbazis_BB.MANGA, DTO.Manga>();
                cfg.CreateMap<Adatbazis_BB.TRANSLATER, DTO.Translater>().ReverseMap();
                //cfg.CreateMap<Adatbazis_BB.MANGAKA, DTO.Mangaka>();
                //cfg.CreateMap<Adatbazis_BB.PUBLISHER, DTO.Publisher>();
                //cfg.CreateMap<Adatbazis_BB.MTCONN, DTO.MTConn>();
            });
            return config.CreateMapper();
        }
    }
}
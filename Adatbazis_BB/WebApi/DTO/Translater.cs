﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.DTO
{
    public class Translater
    {
        public int tranid { get; set; }
        public string tname { get; set; }
        public int tskill { get; set; }
        public string tlanguage { get; set; }
    }
}
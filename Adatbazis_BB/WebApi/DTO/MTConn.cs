﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.DTO
{
    public class MTConn
    {
        public int connid { get; set; }
        public int tid { get; set; }
        public int mid { get; set; }
    }
}
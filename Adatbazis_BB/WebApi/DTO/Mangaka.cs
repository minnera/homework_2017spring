﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.DTO
{
    public class Mangaka
    {
        public int mangakaid { get; set; }
        public string mangakaname { get; set; }
        public string sex { get; set; }
        public DateTime birthdate { get; set; }
        public string addresscitry { get; set; }
        public string nation { get; set; }
        public int publisherid { get; set; }
    }
}
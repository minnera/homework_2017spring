﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.DTO
{
    public class Publisher
    {
        public int publisherid { get; set; }
        public string publishername { get; set; }
        public string planguage { get; set; }
        public DateTime startdate { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.UI.WebControls;
using AutoMapper;
using BusinessLogic;

namespace WebApi.Controllers
{
    public class TestController : ApiController
    {
        private ILogic logic;
        private IMapper mapper;

        public TestController()
        {
            logic = new RealLogic();
            mapper = DTO.AutoMapperConfig.GetMapper();
        }
        // /api/test/translater/43990
        [HttpGet]
        [ActionName("translater")]
        public DTO.Translater GetSingleTranslater(int id)
        {
            Adatbazis_BB.TRANSLATER translater = logic.GetOneTranslater(id);
            return mapper.Map<Adatbazis_BB.TRANSLATER, DTO.Translater>(translater);
        }
        // /api/test/translaters
        [HttpGet]
        [ActionName("translaters")]
        public List<DTO.Translater> GetTranslaters()
        {
            IQueryable<Adatbazis_BB.TRANSLATER> list = logic.GetTranslaters();
            return mapper.Map<IQueryable<Adatbazis_BB.TRANSLATER>, List<DTO.Translater>>(list);
        }
        // /api/test/add
        [HttpPost]
        [ActionName("add")]
        public List<DTO.Translater> AddTranslater(DTO.Translater t)
        {
            //kivétel kezelés kéne még ide
            Adatbazis_BB.TRANSLATER realTranslater = mapper.Map<DTO.Translater, Adatbazis_BB.TRANSLATER>(t);
            logic.AddTranslater(realTranslater);
            IQueryable<Adatbazis_BB.TRANSLATER> list = logic.GetTranslaters();
            return mapper.Map<IQueryable<Adatbazis_BB.TRANSLATER>, List<DTO.Translater>>(list);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using BusinessLogic;
using WebApi.Models;

namespace WebApi.Controllers
{
    public class CrudController : Controller
    {
        private IMapper mapper;
        private ILogic logic;
        private CrudModel model;

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            mapper = DTO.AutoMapperConfig.GetMapper();

            logic = new RealLogic();
            model = new CrudModel();
            model.EditObject = new TranslaterModel();

            var tlist = logic.GetTranslaters();
            model.List = mapper.Map<IQueryable<Adatbazis_BB.TRANSLATER>, List<Models.TranslaterModel>>(tlist);

            base.OnActionExecuting(filterContext);
        }

        private TranslaterModel GetTranslaterModel(int id)
        {
            Adatbazis_BB.TRANSLATER t = logic.GetOneTranslater(id);
            return mapper.Map<Adatbazis_BB.TRANSLATER, Models.TranslaterModel>(t);
        }

        // GET: Crud
        public ActionResult Index()
        {
            ViewData["TargetAction"] = "AddNew";

            return View("CrudIndex", model);
        }

        public ActionResult Details(int id)
        {
            return View("CrudDatasheet", GetTranslaterModel(id));
        }
        public ActionResult Edit(int id)
        {
            ViewData["TargetAction"] = "EditAll";
            model.EditObject = GetTranslaterModel(id);
            return View("CrudIndex", model);
        }
        [HttpPost]
        public ActionResult EditAll(TranslaterModel model)
        {
            if (ModelState.IsValid && model != null)
            {
                logic.ModifyTranslaterAll(model.tranid, model.tname, model.tskill, model.tlanguage);
                TempData["result"] = "Edit OK";
            }
            else
            {
                TempData["result"] = "Edit FAIL";
            }
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult EditLang(TranslaterModel model)
        {
            if (ModelState.IsValid && model != null)
            {
                logic.ModifyTranslaterLang(model.tranid, model.tskill, model.tlanguage);
                TempData["result"] = "Edit OK";
            }
            else
            {
                TempData["result"] = "Edit FAIL";
            }
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult EditName(TranslaterModel model)
        {
            if (ModelState.IsValid && model != null)
            {
                logic.ModifyTranslaterName(model.tranid, model.tname);
                TempData["result"] = "Edit OK";
            }
            else
            {
                TempData["result"] = "Edit FAIL";
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult EditSkill(TranslaterModel model)
        {
            if (ModelState.IsValid && model != null)
            {
                logic.ModifyTranslaterSkill(model.tranid);
                TempData["result"] = "Edit OK";
            }
            else
            {
                TempData["result"] = "Edit FAIL";
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult AddNew(TranslaterModel model)
        {
            if (ModelState.IsValid && model != null)
            {
                Adatbazis_BB.TRANSLATER t = mapper.Map<Models.TranslaterModel, Adatbazis_BB.TRANSLATER>(model);
                t.TRANID = logic.GetNextTransno();
                logic.AddTranslater(t);
                TempData["result"] = "Add OK";
            }
            else
            {
                TempData["result"] = "Add FAIL";
            }
            return RedirectToAction("Index");
        }
        public ActionResult Remove(int id)
        {
            try
            {
                logic.DelTrans(id);
                TempData["result"] = "Del OK";
            }
            catch (DbUpdateException)
            {
                TempData["result"] = "Del FAIL";
            }
            catch (ArgumentException)
            {
                TempData["result"] = "Del FAIL";
            }
            return RedirectToAction("Index");
        }
    }
}
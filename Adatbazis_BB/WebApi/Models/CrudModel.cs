﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class CrudModel
    {
        public List<TranslaterModel> List { get; set; }
        public TranslaterModel EditObject { get; set; }
    }
}
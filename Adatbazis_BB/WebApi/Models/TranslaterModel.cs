﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class TranslaterModel
    {
        [Display(Name = "Translater ID")]
        [Required]//kötelező megadni!
        public int tranid { get; set; }
        [Display(Name = "Translater Name")]
        [Required]
        [StringLength(42, MinimumLength = 2)]
        public string tname { get; set; }
        [Display(Name = "Translater Language")]
        [Required]
        [StringLength(20, MinimumLength = 2)]//max 20 karakter, min. 5
        public string tlanguage { get; set; }
        [Display(Name = "Translater Skill")]
        [Required]
        public int tskill { get; set; }
    }
}
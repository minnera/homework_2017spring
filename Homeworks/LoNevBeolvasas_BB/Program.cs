﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Lovas;
using System.IO;
using System.Text.RegularExpressions;

namespace LoNevBeolvasas_BB
{
    class Program
    {
        const string FileName = "lonevek.xml";
        static void Main(string[] args)
        {
            XmlSerializer xml = new XmlSerializer(typeof(List<Horse>));
            List<Horse> list = new List<Horse>();
            if (File.Exists(FileName))
            {
                Console.WriteLine("Beolvasás fájlból");
                using(FileStream fs = File.Open(FileName, FileMode.Open))
                {
                    list = xml.Deserialize(fs) as List<Horse>;
                    foreach(Horse h in list)
                    {
                        Console.WriteLine(h.Name);
                    }
                }
            }
            else
            {
                Console.WriteLine("Fájlba írás");
                using(FileStream fs = File.Create(FileName))
                {
                    for (int j = 0; j < 3; j++)
                    {
                        Console.WriteLine("Adja meg a következő ló nevét (formátum: 3 különálló szó pontosan, egy szó min. 3, max. 12 karakteres, elválasztás space vagy kötőjel):");
                        string seged = Console.ReadLine();

                        string regexname = @"^([A-ZÉÁŐÚŰÖÜÓ][a-zéáőúűöüó]{2,11})(( |-)([A-ZÉÁŐÚŰÖÜÓ][a-zéáőúűöüó]{2,11})){2}";
                        //az első részt ne mentsük le.

                        Horse h;
                        if (Regex.IsMatch(seged, regexname))
                        {
                            //levágjuk az első részét
                            int i = 0;
                            while (seged[i] != ' ' && seged[i] != '-')
                            {
                                i++;
                            }
                            //most segednek az i-edik indexe az első space vagy -
                            //ami nekünk kell: i+1 kezdőindextől seged végéig
                            seged = seged.Remove(0, i + 1);
                            h = new Horse(seged);
                        }
                        else
                        {
                            h = new Horse();
                        }
                        list.Add(h);
                    }

                    xml.Serialize(fs, list);
                }

                
            }


            Console.ReadLine();
        }
    }
}

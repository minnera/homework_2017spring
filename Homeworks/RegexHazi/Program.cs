﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.IO;

namespace RegexHazi
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Adj meg egy MAC címet:");
            string mac = Console.ReadLine();
            mac = mac.ToUpper();
            string regex_max = @"^((([A-F0-9]{2}-){5})|(([A-F0-9]{2}:){5}))([A-F0-9]{2})$";

            if (Regex.IsMatch(mac, regex_max))
            {
                string back = mac.Substring(15) + mac.Substring(2, 13) + mac.Substring(0, 2);
                Console.WriteLine(back);
                using (StreamWriter sr = new StreamWriter("helyes.txt", true))
                {
                    sr.WriteLine(mac);
                }
            }
            else
            {
                using (StreamWriter sr = new StreamWriter("hibas.txt", true))
                {
                    sr.WriteLine(mac);
                }
            }
            Console.ReadLine();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lovas
{
    public class RacingEventArgs : EventArgs
    {
        private int _helyezes;

        private string _name;

        public int Helyezes
        {
            get
            {
                return _helyezes;
            }

            set
            {
                _helyezes = value;
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lovas
{
    class Pony : Horse
    {
        public Pony() : base()
        {

        }

        public Pony(string name) : base(name)
        {

        }

        public Pony(string name, double price) : base(name, price)
        {

        }
        public override void Whinny()
        {
            Console.WriteLine("Pfrrt");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lovas
{
    class Program
    {
        static void Main(string[] args)
        {
            Horse h1 = new Horse("Zy", 200);
            Horse h2 = new Horse("Aab", 100);

            Horse h3 = new Horse("Kincsem", 20000);
            Horse h4 = new Horse("Herceg", 10000.0001);

            Listener l1 = new Listener("Béla");
            Listener l2 = new Listener("Kata");

            h1.Race += l1.Bettor;
            h2.Race += l1.Bettor;

            h2.Race += l2.Bettor;

            List<Horse> horselist = new List<Horse>();
            horselist.Add(h1);
            horselist.Add(h2);
            horselist.Add(h3);
            horselist.Add(h4);


            Console.WriteLine("Név szerint listázva a lovak:");
            horselist = Horse.SortedbyName(horselist);
            Console.WriteLine(horselist[0].Name);
            Console.WriteLine(horselist[1].Name);
            Console.WriteLine(horselist[2].Name);
            Console.WriteLine(horselist[3].Name);

            Console.WriteLine();

            Console.WriteLine("Születési dátum szerint listázva a lovak:");
            horselist = Horse.SortedbyBirthDate(horselist);
            Console.WriteLine(horselist[0].Name + " " + horselist[0].Birthdate);
            Console.WriteLine(horselist[1].Name + " " + horselist[1].Birthdate);
            Console.WriteLine(horselist[2].Name + " " + horselist[2].Birthdate);
            Console.WriteLine(horselist[3].Name + " " + horselist[3].Birthdate);

            Console.WriteLine();

            Console.WriteLine("Ár szerint listázva a lovak:");
            horselist = Horse.SortedbyPrice(horselist);
            Console.WriteLine(horselist[0].Name + " " + horselist[0].Price);
            Console.WriteLine(horselist[1].Name + " " + horselist[1].Price);
            Console.WriteLine(horselist[2].Name + " " + horselist[2].Price);
            Console.WriteLine(horselist[3].Name + " " + horselist[3].Price);

            Console.WriteLine();

            Console.WriteLine("A 10.000-nél drágább lovak:");

            List<Horse> expensives = new List<Horse>();
            expensives = Horse.Expensive(horselist);

            foreach (Horse h in expensives)
            {
                Console.WriteLine(h.Name + " " + h.Price);
            }

            Console.WriteLine();

            Console.WriteLine("A teszt lólistánknak adunk cukrot, így a lovak nyerítenek:");

            RidingHall<Horse> ridinghall = new RidingHall<Horse>();

            ridinghall.Add(h1);
            ridinghall.Add(h2);
            ridinghall.Add(h3);
            ridinghall.Add(h4);

            Pony p1 = new Pony("Dunci", 444);
            Pony p2 = new Pony("Zord", 11111);

            ridinghall.Add(p1);
            ridinghall.Add(p2);

            ridinghall.GiveCutSugar();

            Console.WriteLine();

            Console.WriteLine("Lóból kolbász, és a keletkezett kolbász ára:");

            Sausage s1 = new Sausage();
            s1 = (Sausage)h1;
            Sausage s2 = new Sausage();
            s2 = (Sausage)p2;

            Console.WriteLine(h1.Price + " > " + s1.Price);
            Console.WriteLine(p2.Price + " > " + s2.Price);

            Console.WriteLine();

            foreach (Horse h in ridinghall)
            {
                Console.WriteLine(h.Name + " versenyzik:");
                h.Racing();
                Console.WriteLine();
            }

            Console.ReadLine();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lovas
{
    public class Medal
    {
        private string _name;

        public string Name
        {
            get
            {
                return _name;
            }
        }

        public int Helyezes
        {
            get
            {
                return _helyezes;
            }

            set
            {
                _helyezes = value;
            }
        }

        private int _helyezes;

        public Medal()
        {
            _name = "Anonymus";
            _helyezes = 0;
        }

        public Medal(string name)
        {
            _name = name;
            _helyezes = 1;
        }

        public Medal(string name, int i)
        {
            _name = name;
            _helyezes = i;
        }
    }
}

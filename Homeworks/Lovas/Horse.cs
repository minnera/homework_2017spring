﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lovas
{
    
    public class Horse : IComparable<Horse>
    {
        private string _name;
        private DateTime _birthdate;
        private double _price = 0.0; //olyan ár lenne jobb, ami null-t is fel tud venni, int? típussal.
        //lista díjakról
        private List<Medal> _medals;
        public double Price
        {
            get
            {
                return _price;
            }
            set
            {
                _price = value;
            }
        }

        public DateTime Birthdate
        {
            get
            {
                return _birthdate;
            }
            set { _birthdate = value; }
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set { _name = value; }
        }

        public List<Medal> Medals
        {
            get
            {
                return _medals;
            }
        }

        public Horse()
        {
            _name = "Felvert Por";
            _birthdate = DateTime.Now;
            _medals = new List<Medal>();
        }

        public Horse(string nick)
        {
            _name = nick;
            _birthdate = DateTime.Now;
            _medals = new List<Medal>();
        }

        public Horse(string nick, double price)
        {
            _name = nick;
            _birthdate = DateTime.Now;
            _price = price;
            _medals = new List<Medal>();
        }

        public event EventHandler<RacingEventArgs> Race;

        public void Racing()
        {
            //feldobja a Race eseményt
            Random rnd = new Random();

            RacingEventArgs rea = new RacingEventArgs();
            rea.Helyezes = rnd.Next(0,11);
            rea.Name = this.Name;
            if(Race != null)
                Race(this, rea);
        }

        static public List<Horse> SortedbyName(List<Horse> list)
        {
            List<Horse> sorted = list;
            sorted.Sort();
            return sorted;
        }
        static public List<Horse> SortedbyBirthDate(List<Horse> list)
        {
            List<Horse> sorted = list;
            sorted.Sort(delegate(Horse a, Horse b) { return a.Birthdate.CompareTo(b.Birthdate); });
            return sorted;
        }

        static public List<Horse> SortedbyPrice(List<Horse> list)
        {
            List<Horse> sorted = list;
            sorted.Sort(new HorsePriceComparer());
            return sorted;
        }

        public int CompareTo(Horse other)
        {
            return this.Name.CompareTo(other.Name);
        }

        public static List<Horse> Expensive(List<Horse> list)
        {
            List<Horse> morethan10000 = new List<Horse>();

            foreach (Horse h in list)
            {
                if (h.Price > 10000.0)
                {
                    morethan10000.Add(h);
                }
            }

            return morethan10000;
        }

        public virtual void Whinny()
        {
            Console.WriteLine("Nyihahahaa");
        }

        public static explicit operator Sausage(Horse h)
        {
            Sausage s = new Sausage(h.Price / 2);

            return s;
        }
    }

    class HorsePriceComparer : IComparer<Horse>
    {
        public int Compare(Horse x, Horse y)
        {
            return x.Price.CompareTo(y.Price);
        }
    }
}

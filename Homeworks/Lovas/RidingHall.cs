﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lovas
{
    class RidingHall<T> : IEnumerable<T>
        where T : Horse
    {
        private T[] _items = new T[5];
        private int _nextIndex = 0;//a következő üres helyre mutat a tömbben.

        public void Add(T item)
        {
            if (_nextIndex >= _items.Length)//megnézzük, hogy betelt-e a rendelkezésre álló tömb
            {
                Array.Resize(ref _items, _items.Length * 2);//ha igen, átméretezzük a tömböt dupla akkorára
            }
            _items[_nextIndex] = item;
            _nextIndex++;
        }

        //ez kb a Get/Set method, visszaadja/beállítja a megadott indexű elemet a tömbből/tömbben
        public T this[int index]
        {
            get
            {
                if (index >= _nextIndex)//megnézzük nincs e túlindexelés
                    throw new ArgumentOutOfRangeException("Ez az index még nem létezik.");
                return _items[index];
            }
            set
            {
                if (index >= _nextIndex)//megnézzük nincs e túlindexelés
                    throw new ArgumentOutOfRangeException("Ez az index még nem létezik.");
                _items[index] = value;
            }
        }

        public IEnumerator<T> GetEnumerator()//mikor pl. foreachben végig megyünk a tömbbön, ez végzi el, hogy sorban visszaadja az értékeket
        {
            for (int i = 0; i < _nextIndex; i++)
            {
                yield return _items[i];
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void GiveCutSugar()
        {
            foreach (Horse h in this)
            {
                h.Whinny();
            }
        }
    }
}

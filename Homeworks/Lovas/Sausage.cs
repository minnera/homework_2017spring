﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lovas
{
    public class Sausage
    {
        private double _price;

        public Sausage()
        {
            _price = 1000;
        }

        public Sausage(double price)
        {
            _price = price;
        }

        public double Price
        {
            get
            {
                return _price;
            }
            
        }
    }
}

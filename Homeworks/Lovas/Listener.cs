﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lovas
{
    class Listener
    {
        private string _name;

        public Listener(string name)
        {
            _name = name;
        }

        public void Bettor(object h, RacingEventArgs rea)
        {
            Console.WriteLine("{0} fogadó fogadott {1} nevű lóra.", _name, rea.Name);
            //csinál valamit, ha történik event
            if (rea.Helyezes < 4)
            {
                Console.WriteLine("{0} Nyert", rea.Name);
            }
            else
            {
                Console.WriteLine("{0} Vesztett", rea.Name);
            }
        }

    }
}

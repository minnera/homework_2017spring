﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gyakorlo2
{
    class Lo
    {
        private string _nev;

        private int _atlagsebesseg;//km/h

        public string Nev
        {
            get
            {
                return _nev;
            }

            set
            {
                _nev = value;
            }
        }

        public int Atlagsebesseg
        {
            get
            {
                return _atlagsebesseg;
            }

            set
            {
                _atlagsebesseg = value;
            }
        }

        public Lo()
        {
            _nev = "Senki";
            _atlagsebesseg = 42;
        }
    }
}

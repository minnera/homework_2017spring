﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gyakorlo2
{
    class Tortszam
    {
        private int _szamlalo;
        private int _nevezo;

        public int Szamlalo
        {
            get
            {
                return _szamlalo;
            }

            set
            {
                _szamlalo = value;
            }
        }

        public int Nevezo
        {
            get
            {
                return _nevezo;
            }

            set
            {
                _nevezo = value;
            }
        }

        public Tortszam()
        {

        }

        public Tortszam(int a, int b)
        {
            _szamlalo = a;
            _nevezo = b;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vizora_BB
{
    class Vizora
    {
        private string _name;

        private double _jelenlegiAllas;

        private DateTime _utoljaraBeallitva;

        private List<double> _allasok;

        private List<DateTime> _beallitasok;

        private int _mentettAllasokSzama;

        public string Name
        {
            get
            {
                return _name;
            }
        }

        public double JelenlegiAllas
        {
            get
            {
                return _jelenlegiAllas;
            }

            set
            {
                _jelenlegiAllas = value;
            }
        }

        public DateTime UtoljaraBeallitva
        {
            get
            {
                return _utoljaraBeallitva;
            }
        }

        public Vizora(string name)
        {
            this._allasok = new List<double>();
            this._beallitasok = new List<DateTime>();
            this._name = name;
            this.JelenlegiAllas = 0.0;
            this._utoljaraBeallitva = DateTime.Now;
            this._mentettAllasokSzama = 0;
        }


        public string VizoraLeirasa()
        {
            //vízóra neve, utolsó állása, utolsó állás beállításának ideje: év.hó.nap. óra:perc:másodperc
            //+ az előző állások és idejük, utoljára a legöregebb állást írja ki

            StringBuilder kiir = new StringBuilder(this.Name + ": " + this.JelenlegiAllas.ToString() + " m3, dátum: " + this.UtoljaraBeallitva);

            if (this._mentettAllasokSzama == 1)
            {
                kiir.Append("\nKorábbi állások:\nNincsenek korábbiak");
                return kiir.ToString();
            }

            kiir.Append("\nKorábbi állások:");

            for (int i = this._mentettAllasokSzama - 1; i >= 0; i--)
            {
                kiir.Append("\n" + this._allasok[i].ToString("000.0#") + " m3,\tdátum: " + this._beallitasok[i]);
            }
            return kiir.ToString();
        }

        public void UjAllasHozzaadasa(double ujAllas)
        {
            this._beallitasok.Add(this.UtoljaraBeallitva);
            this._utoljaraBeallitva = DateTime.Now;
            this._allasok.Add(this.JelenlegiAllas);
            this.JelenlegiAllas = ujAllas;
            this._mentettAllasokSzama++;
        }
    }
}
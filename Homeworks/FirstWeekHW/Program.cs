﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vizora_BB
{
    class Program
    {
        enum Menu
        {
            Ujora, Kivalaszt, Kiirat, Ujallas, Kilepes
        };

        static void UjOra(List<Vizora> lista)
        {
            Console.WriteLine("Adja meg az új vízóra nevét: ");
            string nev = Console.ReadLine();
            Vizora ujvo = new Vizora(nev);
            lista.Add(ujvo);
            Console.Clear();
            Console.WriteLine("Sikeres hozzáadás.");
        }

        static void KiValaszt(ref Vizora s)
        {

        }

        static Menu MenuKivalasztas(Vizora jelenlegi)
        {
            int hiba = -1;
            do
            {
                Console.WriteLine("Jelenleg kiválasztott vízóra : " + jelenlegi.Name);
                Console.WriteLine("\nMit szeretne csinálni?");
                Console.WriteLine("\nVízóra kiválasztása - Kivalaszt");
                Console.WriteLine("Új vízóra - Ujora");
                Console.WriteLine("Új állás hozzáadása a kiválasztott vízórához - Ujallas");
                Console.WriteLine("Kiválasztott vízóra eddigi állásainak kiiratása - Kiirat");
                Console.WriteLine("Kilépés - Kilepes");
                Console.WriteLine("\nEzt választom :");
                string seged = Console.ReadLine();

                Console.Clear();
                //TODO hibakezelés, ha nem megfelelő parancsot ad
            
                try
                {
                    Menu menulesz = (Menu)Enum.Parse(typeof(Menu), seged, true);
                    hiba = 0;
                    return menulesz;
                }
                catch (ArgumentException)
                {
                    Console.WriteLine("Hibás parancs lett megadva.");

                }
            } while (hiba == -1);
            Menu hibas = Menu.Kilepes;
            return hibas;
        }

        static void VizorakKiirasa(List<Vizora> lista)
        {
            int i = 1;
            foreach (Vizora vo in lista)
            {
                Console.WriteLine(i + ". Vízóra : " + vo.Name);
                i++;
            }
        }

        static void AllasHozzaadasa(List<Vizora> vizorak, int kivalasztott)
        {
            double ujallas;//feladatban nem volt kikötés számtani határra, ami lehet egy állás
            Console.WriteLine("Adja meg a kiválasztott vízóra új állását : ");
            if (double.TryParse(Console.ReadLine(), out ujallas))
            {
                vizorak[kivalasztott].UjAllasHozzaadasa(ujallas);
                Console.Clear();
                Console.WriteLine("Sikeres hozzáadás.");
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Nem számot adott meg!");
            }
        }

        static void Main(string[] args)
        {
            //tesztelés
            //Vizora teglaVoros = new Vizora("TéglaVörös");
            //teglaVoros.UjAllasHozzaadasa(101.1);
            //teglaVoros.UjAllasHozzaadasa(222.0);
            //teglaVoros.UjAllasHozzaadasa(242.4);
            //teglaVoros.UjAllasHozzaadasa(275.25);
            //teglaVoros.UjAllasHozzaadasa(300);
            //Console.WriteLine(teglaVoros.VizoraLeirasa());

            //Vizorak tárolása
            List<Vizora> vizorak = new List<Vizora>();
            int vizorakSzama = 0;
            int kivalasztott = -1;
            
            //Menüs
            Menu mn = Menu.Ujora; //mivel még nincs óra az indításkor, rögtön egy új óra felvételével kezd


            while (mn != Menu.Kilepes)
            {
                switch(mn)
                {
                    case Menu.Ujora:
                        UjOra(vizorak);
                        kivalasztott = vizorakSzama;
                        vizorakSzama++;
                        mn = MenuKivalasztas(vizorak[kivalasztott]);
                        break;
                    case Menu.Kivalaszt:
                        VizorakKiirasa(vizorak);
                        Console.WriteLine("Adja meg a sorszámát a kiválasztandó vízórának : ");
                        if(int.TryParse(Console.ReadLine(), out kivalasztott))
                        {
                            if (0 < kivalasztott && kivalasztott <= vizorakSzama)
                            {
                                kivalasztott--;
                                Console.Clear();
                                mn = MenuKivalasztas(vizorak[kivalasztott]);
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("Hibás sorszámot adott meg! A lent felsoroltakból válasszon!\n");
                            }
                        }
                        else
                        {
                            Console.Clear();
                            Console.WriteLine("Nem számot adott meg!\n");
                        }
                        
                        break;
                    case Menu.Ujallas:
                        AllasHozzaadasa(vizorak, kivalasztott);
                        mn = MenuKivalasztas(vizorak[kivalasztott]);
                        break;
                    case Menu.Kiirat:
                        Console.WriteLine("Jelenleg kiválasztott vízóra összes állásának kiírása (nyomjon entert a menübe lépéshez) : ");
                        Console.WriteLine(vizorak[kivalasztott].VizoraLeirasa());
                        Console.ReadLine();
                        Console.Clear();
                        mn = MenuKivalasztas(vizorak[kivalasztott]);
                        break;
                    default:
                        mn = Menu.Kilepes;
                        break;
                }
            }
            
        }
    }
}
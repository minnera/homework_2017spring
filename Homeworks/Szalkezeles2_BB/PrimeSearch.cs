﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Szalkezeles2_BB
{
    class PrimeSearch
    {
        private List<int> _numbers;
        private List<int>_primes;

        public List<int> Numbers
        {
            get { return _numbers; }
            set { _numbers = value; }
        }

        public List<int> Primes
        {
            get { return _primes; }
            set { _primes = value; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gyakorlo1
{
    class Program
    {
        public delegate string SajatDelegateTwoIntToString(int a, int b);
        static void Main(string[] args)
        {
            SajatOsztaly so = new SajatOsztaly();

            so.Delegatolo(delegate(int n, int m) { return n.ToString() + m.ToString(); }, 3);

            Console.ReadLine();
        }
    }
}

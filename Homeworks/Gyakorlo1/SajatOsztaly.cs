﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gyakorlo1
{
    class SajatOsztaly : ISajat
    {
        List<int> _lista;

        public SajatOsztaly()
        {
            _lista = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        }

        public void Delegatolo(Program.SajatDelegateTwoIntToString de, int n)
        {
            string s = de(_lista[n], _lista[n+1]);
            Console.WriteLine("Saját Delegát Eredménye : " + s);
        }
    }
}
